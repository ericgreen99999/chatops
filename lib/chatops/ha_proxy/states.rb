# frozen_string_literal: true

module Chatops
  module HAProxy
    module State
      READY = 'ready'
      DRAIN = 'drain'
      MAINT = 'maint'
    end
  end
end
